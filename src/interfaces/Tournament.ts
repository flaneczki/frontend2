export interface Tournament {
  id: number;
  name: string;
  location: string;
  date: Date;
}
