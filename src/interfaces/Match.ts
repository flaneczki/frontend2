export interface Match {
  id: number;
  tournamentId: number;
  createdAt: Date;
  updatedAt: Date;
}
