export interface Player {
  id: number;
  name: string;
  city: string;
  photo: string;
  createdAt: Date;
  updatedAt: Date;
}
