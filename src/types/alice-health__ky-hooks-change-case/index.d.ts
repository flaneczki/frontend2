declare module '@alice-health/ky-hooks-change-case' {
  function createRequestModify(modifier: (string) => string): BeforeRequestHook
  function createResponseModify(modifier: (string) => string): AfterResponseHook
  function requestToCamelCase(): BeforeRequestHook
  function requestToKebabCase(): BeforeRequestHook
  function requestToSnakeCase(): BeforeRequestHook
  function responseToCamelCase(): AfterResponseHook
  function responseToKebabCase(): AfterResponseHook
  function responseToSnakeCase(): AfterResponseHook
}
