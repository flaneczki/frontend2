import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import Home from '../views/Home.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/tournaments',
    name: 'Tournaments',
    component: () => import(/* webpackChunkName: "tournaments" */ '../views/Tournaments.vue'),
  },
  {
    path: '/tournaments/:tournamentId',
    name: 'TournamentDetails',
    component: () => import(/* webpackChunkName: "details" */ '../views/TournamentDetails.vue'),
    props: true,
  },
  {
    path: '/players',
    name: 'Players',
    component: () => import(/* webpackChunkName: "players" */ '../views/Players.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
