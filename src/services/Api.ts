import ky from 'ky';
import {
  requestToSnakeCase,
  responseToCamelCase,
} from '@alice-health/ky-hooks-change-case';

const Api = ky.create({
  prefixUrl: 'https://incognito.frelia.org/api/v1',
  hooks: {
    beforeRequest: [
      requestToSnakeCase,
      (request) => {
        console.log(request);
        request.text().then((text) => { console.log(`Request body: ${text}`); });
      },
    ],
    afterResponse: [
      responseToCamelCase,
      (_request, _options, response) => {
        console.log(response);
        response.text().then((text) => { console.log(`Body: ${text}`); });
      },
    ],
  },
});

export default Api;
