import Api from '@/services/Api';
import { Player } from '@/interfaces/Player';

export default {
  async getPlayers(): Promise<Player[]> {
    const players: Player[] = await Api.get('/api/v1/players').json();
    return players;
  },
};
