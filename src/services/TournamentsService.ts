import Api from '@/services/Api';
import { Tournament } from '@/interfaces/Tournament';

export default {
  async getTournaments(): Promise<Tournament[]> {
    const tournaments: Tournament[] = await Api.get('/tournaments').json();
    return tournaments;
  },
};
