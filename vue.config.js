module.exports = {
  lintOnSave: process.env.NODE_ENV !== 'production',
  devServer: {
    host: '0.0.0.0',
    allowedHosts: [
      '.gitpod.io',
      'https://incognito.frelia.org',
    ],
    client: {
      webSocketURL: process.env.GITPOD_WORKSPACE_URL
        ? `${process.env.GITPOD_WORKSPACE_URL.replace('https://', 'wss://8080-')}/ws`
        : 'ws://0.0.0.0:8080/ws',
    },
  },
};
